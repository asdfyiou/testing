# Would expect this image to be used for _all_ R processes I guess, or perhaps it's best to create
# something particular to each particular process not too sure.

FROM rocker/r-ver:4.1.2

# Haven't bothered for this example - but for packages like tidyverse etc would probably install
# them with remotes as follows:

# RUN R -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
# RUN R -e "remotes::install_version('tidyverse',  '1.3.1')"