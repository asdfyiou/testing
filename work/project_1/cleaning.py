
import pandas as pd

def main() -> int:
    data = pd.read_csv('./data/project_1/raw/data.csv')
    # bigger numbers are better
    data = data.add(10)
    data.to_csv('./data/project_1/interim/data.csv')
    return 0


if __name__ == "__main__":
    raise SystemExit(main())