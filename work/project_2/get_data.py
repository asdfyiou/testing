"""
Create data for testing.

Typically might be retrieved from a database/storage/whatever.
"""
import numpy as np
import pandas as pd


def main() -> int:
    rng = np.random.default_rng(1)
    shape = (200, 5)
    data = pd.DataFrame(
        rng.integers(0, 100, size=shape),
        columns=["y"] + [f"x_{i}" for i in range(shape[1] - 1)],
    )
    
    data.to_csv("./data/project_2/raw/data.csv")

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
